import nltk

text = "Cumhurbaşkanı Erdoğan, NATO müttefiklerinin Türkiye ile somut dayanışma sergilemesi gerektiğini vurgulayarak, ittifakın işlevselliğini riske atan teşebbüslere mahal verilmemesi noktasında NATOnun çok öne.mli rolü ve sorumluğu bulunduğuna işaret etti."
words = nltk.word_tokenize(text)

tagged = nltk.pos_tag(words)

ner = nltk.ne_chunk(tagged)

ner.draw()
